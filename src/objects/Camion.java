package objects;

/**
 * Camions
 */
public class Camion extends Vehicule {

   public Camion(int annee, double prix) {
      super(annee, prix);
      // TODO Auto-generated constructor stub
   }

   @Override
   public void demarrer() {
      // TODO Show something

      System.out.println("le camion "+this.getMatricule()+" demarre");

   }

   @Override
   public void accelerer() {
      // TODO Show something


      System.out.println("le camion "+this.getMatricule()+" accelere");

   }

   
}