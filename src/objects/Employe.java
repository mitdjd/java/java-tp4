package objects;

import java.util.Date;

/**
 * Employe
 */
public class Employe extends Personne {

   private double salaire;

   public double getSalaire() {
      return salaire;
   }

   public void setSalaire(double salaire) {
      this.salaire = salaire;
   }

   public Employe(String nom, String prenom, Date birthYear, double salaire) {
      super(nom,prenom,birthYear);
      this.salaire = salaire;
   }

   public void show() {
      //TODO 
      super.show();
      System.out.println("Salaire : "+this.getSalaire());
      
   }
}