package objects;

/**
 * Vehicule
 */
public abstract class Vehicule {

   private static int matRef = 0;
   private int matricule;
   private int annee;
   private double prix;



   public double getPrix() {
      return prix;
   }

   public void setPrix(final double prix) {
      this.prix = prix;
   }

   public int getAnnee() {
      return annee;
   }

   public void setAnnee(final int annee) {
      this.annee = annee;
   }

   public int getMatricule() {
      return matricule;
   }

   public void setMatricule(final int matricule) {
      this.matricule = matricule;
   }



   public Vehicule(int annee, double prix) {
      this.setMatricule(matRef++);
      this.annee = annee;
      this.prix = prix;
   }

   public abstract void demarrer();

   public abstract void accelerer();

   public String toString() {
      return "Matericule : "+this.matricule+" Annee : "+this.annee+" Prix : "+this.prix;
      
   }

   
}