package objects;

import java.util.Date;

/**
 * Directeur
 */
public class Directeur extends Chef {

   private String societe;

   public String getSociete() {
      return societe;
   }

   public void setSociete(String societe) {
      this.societe = societe;
   }

   public Directeur(String nom, String prenom, Date birthYear, double salaire, String service, String societe) {
      super(nom, prenom, birthYear, salaire, service);
      // TODO Auto-generated constructor stub
      this.societe = societe;
   }

   public void show() {
      //TODO 
      super.show();
      System.out.println("societe : "+this.getSociete());
   }
}