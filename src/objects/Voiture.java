package objects;

/**
 * Voitures
 */
public class Voiture extends Vehicule{

   public Voiture(int annee, double prix) {
      super(annee, prix);
      // TODO Auto-generated constructor stub
   }

   @Override
   public void demarrer() {
      // TODO Show something

      System.out.println("la voiture "+this.getMatricule()+" demarre");

   }

   @Override
   public void accelerer() {
      // TODO Show something

      System.out.println("la voiture "+this.getMatricule()+" accelere");

   }

   
}