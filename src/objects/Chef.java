package objects;

import java.util.Date;

/**
 * Chef
 */
public class Chef extends Employe {

   private String service;

   public String getService() {
      return service;
   }

   public void setService(String service) {
      this.service = service;
   }

   public Chef(String nom, String prenom, Date birthYear, double salaire, String service) {
      super(nom, prenom, birthYear, salaire);
      // TODO Auto-generated constructor stub
      this.service = service;
   }

   public void show() {
      //TODO 
      super.show();
      System.out.println("service : "+this.getService());
   }

   
}