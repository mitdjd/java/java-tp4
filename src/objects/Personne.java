package objects;

import java.util.Date;

/**
 * Personne
 */
public class Personne {

   private String nom;
   private String prenom;
   private Date birthDate;

   public Personne(String nom, String prenom, Date birthYear) {
      super();
      this.nom = nom;
      this.prenom= prenom;
      this.birthDate = birthYear;
   }

   public String getNom() {
      return this.nom;
   }

   public void setNom(final String nom) {
      this.nom = nom;
   }

   public String getPrenom() {
      return this.prenom;
   }

   public void setPrenom(final String prenom) {
      this.prenom = prenom;
   }

   public Date getBirthYear() {
      return this.birthDate;
   }

   public void setBirthYear(final Date birthYear) {
      this.birthDate = birthYear;
   }

   public void show() {
      //TODO 
      System.out.println();
      System.out.println("nom : "+this.nom);
      System.out.println("prenom : "+this.prenom);
      System.out.println("birthyear : "+this.birthDate);
   }
}