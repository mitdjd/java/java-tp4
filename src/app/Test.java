

package app;

import objects.Camion;
import objects.Voiture;


/**
 * Text
 */
public class Test {

   public static void main(final String[] args) {
      Voiture voiture = new Voiture(2020,250000);
      Camion camion = new Camion(2020,250000);
      voiture.accelerer();
      voiture.demarrer();
      //
      camion.accelerer();
      camion.demarrer();

      //
      Camion camion2 = new Camion(2020,250000);
      camion.accelerer();
      camion.demarrer();
      camion2.accelerer();
      camion2.demarrer();
   }
}