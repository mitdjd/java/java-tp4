package app.exceptions;

/**
 * TestException
 */
public class TestException extends Exception {

   public TestException(String message) {
      super(message);
   }
}