package app;

import java.text.SimpleDateFormat;
import java.util.Date;

import objects.Chef;
import objects.Directeur;
import objects.Employe;
import objects.Personne;

import java.text.ParseException;

/**
 * Test2
 */
public class Test2 {

   public static void main(String[] args) throws Exception {
      
      //
      Personne[] personnes = new Personne[8];
      //
      personnes[0] = new Employe("HAd", "youssef", Test2.convertDate(2020, 1, 1) , 0);
      personnes[1] = new Employe("HAd1", "youssef1", Test2.convertDate(2020, 1, 1) , 0);
      personnes[2] = new Employe("Had2", "Youssef2", Test2.convertDate(2020, 1, 1) , 0);
      personnes[3] = new Employe("Had3", "Youssef3", Test2.convertDate(2020, 1, 1) , 0);
      personnes[4] = new Employe("Had4", "Youssef4", Test2.convertDate(2020, 1, 1) , 0);
      //
      personnes[5] = new Chef("Had5", "Youssef5", Test2.convertDate(2020, 1, 1) ,0, "service");
      personnes[6] = new Chef("Had6", "Youssef6", Test2.convertDate(2020, 1, 1) ,0, "service tels");
      //
      personnes[7] = new Directeur("Had6", "Youssef6", Test2.convertDate(2020, 1, 1) ,0, "service tels", "direction tels");
      //
      for (int i = 0; i < personnes.length; i++) {
         personnes[i].show();
      }
      //
      for (Personne personne : personnes) {
         personne.show();
      }      
   }

   public static Date convertDate(int year, int month, int day) throws Exception {
      // throw new ParseException();
      SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
      String dateString = month+"/"+day+"/"+year;
      Date date = sdf.parse(dateString);
      return date;
   }
}